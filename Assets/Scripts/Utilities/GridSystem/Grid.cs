﻿using System;
using UnityEngine;

namespace Utilities.GridSystem
{
    // ReSharper disable once InconsistentNaming
    public class Grid<TileType>
    {
        private readonly Vector2Int _gridSize;
        private readonly TileType[,] _gridTiles;

        public Grid(Vector2Int size)
        {
            if (size.x <= 0 || size.y <= 0)
            {
                throw  new ArgumentException(
                    "Grid height/length can not be zero.", nameof(size));
            }
            
            _gridSize = size;
            _gridTiles = new TileType[size.x, size.y];
        }

        public TileType GetTile(Vector2Int index)
        {
            HandleIndexInput(index);

            return _gridTiles[index.x, index.y];
        }

        public void SetTile(TileType tile, Vector2Int index)
        {
            HandleIndexInput(index);

            _gridTiles[index.x, index.y] = tile;
        }


        private void HandleIndexInput(Vector2Int index)
        {
            if (index.x > _gridSize.x - 1 || index.x < 0 ||
                index.y > _gridSize.y - 1 || index.y < 0)
            {
                throw new ArgumentException(
                    "Index was out of Range!", nameof(index));
            }
        }
    }
}