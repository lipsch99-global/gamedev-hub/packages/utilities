﻿using System;
using NUnit.Framework;
using UnityEngine;
using Utilities.GridSystem;
// ReSharper disable ObjectCreationAsStatement

namespace Utilities.Tests.GridSystem
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class GridTests
    {
        public class Constructor
        {
            [Test]
            public void Should_Throw_Exception_If_Height_Of_Grid_Is_Zero()
            {
                Assert.Throws<ArgumentException>(
                    () => { new Grid<int>(new Vector2Int(5, 0)); }
                );
            }

            [Test]
            public void Should_Throw_Exception_If_Length_Of_Grid_Is_Zero()
            {
                Assert.Throws<ArgumentException>(
                    () => { new Grid<int>(new Vector2Int(0, 5)); }
                );
            }
        }

        public class GetTile
        {
            [Test]
            public void Should_Throw_Exception_If_Index_Is_Outside_Grid_Size()
            {
                var grid = new Grid<int>(new Vector2Int(5, 5));
                Assert.Throws<ArgumentException>(
                    () => { grid.GetTile(new Vector2Int(5, 4)); }
                );
            }
        }

        public class SetTile
        {
            [Test]
            public void Tile_Should_Be_Set_And_Retrieved_Successfully()
            {
                var grid = new Grid<int>(new Vector2Int(5, 5));
                var tileIndex = new Vector2Int(0, 0);
                const int tileValue = 5;
                grid.SetTile(tileValue, tileIndex);
                Assert.AreEqual(tileValue,grid.GetTile(tileIndex));
            }
            
            [Test]
            public void Should_Throw_Exception_If_Index_Is_Outside_Grid_Size()
            {
                var grid = new Grid<int>(new Vector2Int(5, 5));
                Assert.Throws<ArgumentException>(
                    () => { grid.SetTile(1,new Vector2Int(5, 4)); }
                );
            }
        }
    }
}